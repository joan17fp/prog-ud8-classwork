package prog.Activitat3;

 abstract public class Vehicle {

    protected String matricula;

    protected String color;

    protected String marca;

    protected String model;


    public Vehicle(String matricula, String color, String marca, String model) {

        this.matricula = matricula;
        this.color = color;
        this.marca = marca;
        this.model = model;

    }

    public String getMatricula() {

        return matricula;

    }


    @Override
    public String toString() {

        return "Matricula: " + matricula + "\nColor: " + color + "\nMarca: " + marca + "\nModel: " + model;
    }


}
