package prog.Activitat3;

public class Coche extends Vehicle {

        private int cilindrada;

        private int numPuertas;

    public Coche(String matricula, String color, String marca, String model, int cilindrada, int numPuertas) {
        super(matricula, color, marca, model);
        this.cilindrada = cilindrada;
        this.numPuertas = numPuertas;
    }


        public String getMatricula() {

            return matricula;

        }


        @Override
        public String toString() {

            return "Matricula: " + matricula + "\nColor: " + color + "\nMarca: " + marca + "\nModel: " + model + "\nCilindrada: " + cilindrada
                    + "\nNumero de puertas: " + numPuertas;
        }
}
