package prog.Activitat3;


    public class Moto extends Vehicle {

        private int cilindrada;

        public Moto(String matricula, String color, String marca, String model, int cilindrada) {
            super(matricula, color, marca, model);
            this.cilindrada = cilindrada;
        }


        public String getMatricula() {

            return matricula;

        }


        @Override
        public String toString() {

            return "Matricula: " + matricula + "\nColor: " + color + "\nMarca: " + marca + "\nModel: " + model + "\nCilindrada: ";
        }
    }