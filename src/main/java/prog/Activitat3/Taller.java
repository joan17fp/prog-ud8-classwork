package prog.Activitat3;

import java.util.ArrayList;
import java.util.Scanner;

public class Taller {

    private ArrayList<Vehicle> vehiclesPendientes = new ArrayList<>();

    private ArrayList<Vehicle> vehiclesReparados = new ArrayList<>();

    private Scanner lector = new Scanner(System.in);

    Recepció recepció = new Recepció();

    public void repararVehicles() {

        System.out.println("Escribe la matricula del vehiculo a retirar:");
        String vehicleSelected = lector.next();

        Vehicle searchedVehicle = searchPendientes(vehicleSelected);

        if (searchedVehicle != null) {

            vehiclesReparados.add(searchedVehicle);
            vehiclesPendientes.remove(searchedVehicle);

        } else {
            System.out.println("Su vehiculo no se encuentra en el taller");
        }

    }

    public void registerNewVehicle() {

        vehiclesPendientes.add(recepció.selectVehicle());


    }

    public Vehicle searchPendientes(String matricula) {

        for (Vehicle vehicle : vehiclesPendientes) {

            if (vehicle.getMatricula().equalsIgnoreCase(matricula)) {

                return vehicle;

            }

        }

        return null;

    }
    public Vehicle searchReparados(String matricula) {

        for (Vehicle vehicle : vehiclesReparados) {

            if (vehicle.getMatricula().equalsIgnoreCase(matricula)) {

                return vehicle;

            }

        }

        return null;

    }

    public void removeVehicle() {

        System.out.println("Escribe la matricula del vehiculo a retirar:");
        String vehicleSelected = lector.next();
        Vehicle searchedVehicle = searchReparados(vehicleSelected);
        if (searchedVehicle != null) {
            vehiclesReparados.remove(searchedVehicle);
            System.out.println("Su vehiculo ha sido retirado");

        } else {
            System.out.println("Su vehiculo no se encuentra en el taller");
        }
    }

    public void listaRegistrados() {

        if (vehiclesPendientes.size() != 0) {
            System.out.println(vehiclesPendientes);
        } else {
            System.out.println("Aún no hay ningun coche en la lista");
        }

    }

    public void listaReparados() {

        if (vehiclesReparados.size() != 0) {
            System.out.println(vehiclesReparados);
        } else {
            System.out.println("Aún no hay ningun coche en la lista");
        }

    }

}
