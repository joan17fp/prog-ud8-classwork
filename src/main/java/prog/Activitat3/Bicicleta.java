package prog.Activitat3;

    public class Bicicleta extends Vehicle {

        public Bicicleta(String matricula, String color, String marca, String model) {
            super(matricula, color, marca, model);

        }


        public String getMatricula() {

            return matricula;

        }


        @Override
        public String toString() {

            return "Matricula: " + matricula + "\nColor: " + color + "\nMarca: " + marca + "\nModel: " + model;
        }
    }
