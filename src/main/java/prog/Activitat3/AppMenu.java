package prog.Activitat3;

import java.util.Scanner;

public class AppMenu {

    private final int OPTION_ADD_NEW = 1;
    private final int OPTION_LIST_REGIST = 2;
    private final int OPTION_REPAIR_NEW = 3;
    private final int OPTION_LIST_REPAIRED = 4;
    private final int OPTION_REMOVE_CAR = 5;
    private final int OPTION_EXIT_NEW = 6;


     Taller taller = new Taller();

    private Scanner lector = new Scanner(System.in);


    public void menu() {

        int respuesta;
        System.out.println("╔═════════════════════════════════╗");
        System.out.println("║ Bienvenido a Talleres Marcel·lí ║");
        System.out.println("╚═════════════════════════════════╝");
        System.out.println();

        do {
            System.out.println("------------------------");
            System.out.println("Eliga la opción deseada:");
            System.out.println("------------------------");
            System.out.println(OPTION_ADD_NEW + " -> Registrar su vehiculo en el taller.");
            System.out.println(OPTION_LIST_REGIST + " -> Listado de vehículos pendientes a reparar");
            System.out.println(OPTION_REPAIR_NEW + " -> Reparación de su vehiculo.");
            System.out.println(OPTION_LIST_REPAIRED + " -> Listado de vehiculos reparados");
            System.out.println(OPTION_REMOVE_CAR + " -> Retirar vehiculo del taller");
            System.out.println(OPTION_EXIT_NEW + " -> Salir del menú.");

            respuesta = lector.nextInt();

            switch (respuesta) {
                case OPTION_ADD_NEW:
                    taller.registerNewVehicle();
                    break;
                case OPTION_LIST_REGIST:
                    taller.listaRegistrados();
                    break;
                case OPTION_REPAIR_NEW:
                    taller.repararVehicles();
                    break;
                case OPTION_LIST_REPAIRED:
                    taller.listaReparados();
                    break;
                case OPTION_REMOVE_CAR:
                    taller.removeVehicle();
                    break;
            }

        } while (respuesta != OPTION_EXIT_NEW);
        System.out.println("¡Que tenga un buen dia!");
    }

}

