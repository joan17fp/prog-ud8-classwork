package prog.Activitat3;

import java.util.Scanner;

public class Recepció {

    private Scanner lector = new Scanner(System.in);

    private final int OPTION_COCHE = 1;
    private final int OPTION_BICI = 2;
    private final int OPTION_CAMION = 3;
    private final int OPTION_MOTO = 4;
    private final int OPTION_EXIT = 5;


    public Vehicle selectVehicle() {



        System.out.println("¿Que tipo de vehículo quieres registrar?");
        System.out.println(OPTION_COCHE + " -> Coche");
        System.out.println(OPTION_BICI + " -> Bicicleta");
        System.out.println(OPTION_CAMION + " -> Camión");
        System.out.println(OPTION_MOTO + " -> Moto");
        System.out.println(OPTION_EXIT + " -> Salir");

        int respuesta = lector.nextInt();

        switch (respuesta) {
            case OPTION_COCHE:
                return registerNewCar();
            case OPTION_BICI:
                return registerNewBike();
            case OPTION_CAMION:
                return registerNewTruck();
            case OPTION_MOTO:
                return registerNewMotorcycle();
        }
        return null;
    }


    private Coche registerNewCar() {

        System.out.println("Introduce los datos del coche:");
        System.out.print("Matricula:");
        String matricula = lector.next();
        System.out.print("Color:");
        String color = lector.next();
        System.out.print("Modelo:");
        String modelo = lector.next();
        System.out.print("Marca:");
        String marca = lector.next();
        System.out.print("Cilindrada:");
        int cilindrada = lector.nextInt();
        System.out.print("Número de puertas:");
        int numPuertas = lector.nextInt();

        return new Coche(matricula, color, marca, modelo, cilindrada, numPuertas);
    }

    private Bicicleta registerNewBike() {

        System.out.println("Introduce los datos de la bici:");
        System.out.print("Matricula:");
        String matricula = lector.next();
        System.out.print("Color:");
        String color = lector.next();
        System.out.print("Modelo:");
        String modelo = lector.next();
        System.out.print("Marca:");
        String marca = lector.next();

        return new Bicicleta(matricula, color, marca, modelo);
    }

    private Camion registerNewTruck() {

        System.out.println("Introduce los datos del coche:");
        System.out.print("Matricula:");
        String matricula = lector.next();
        System.out.print("Color:");
        String color = lector.next();
        System.out.print("Modelo:");
        String modelo = lector.next();
        System.out.print("Marca:");
        String marca = lector.next();
        System.out.print("Cilindrada:");
        int cilindrada = lector.nextInt();
        System.out.print("Número de puertas:");
        int numPuertas = lector.nextInt();

        return new Camion(matricula, color, marca, modelo, cilindrada, numPuertas);
    }

    private Moto registerNewMotorcycle() {

        System.out.println("Introduce los datos del coche:");
        System.out.print("Matricula:");
        String matricula = lector.next();
        System.out.print("Color:");
        String color = lector.next();
        System.out.print("Modelo:");
        String modelo = lector.next();
        System.out.print("Marca:");
        String marca = lector.next();
        System.out.print("Cilindrada:");
        int cilindrada = lector.nextInt();

        return new Moto(matricula, color, marca, modelo, cilindrada);
    }
}
