package prog.Activitat4;

public interface Drawable {

  public abstract String getDrawing();

  public abstract String getDrawing(Color color);

  public abstract void setColor(Color color);




}
