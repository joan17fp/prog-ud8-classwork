package prog.Activitat4;

public class Circulo extends Figuras {

    public int radio;
    double pi = 3.14;


    public Circulo(int radio) {
        this.radio = radio;
    }

    @Override
    public String getDrawing() {
        return "O";
    }

    @Override
    public String getDrawing(Color color) {
        return color+"O";
    }

    @Override
    public void setColor(Color color) {

    }

    @Override
    void getArea() {

        System.out.println("Area = "+(pi*(radio*radio)));
    }
    @Override
    void getName() {
        System.out.println("Circulo");
    }

    @Override
    void getPerimetro() {
        System.out.println("Perimetro = "+2*(pi*(radio*radio)));
    }
}