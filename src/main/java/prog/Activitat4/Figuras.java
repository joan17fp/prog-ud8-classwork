package prog.Activitat4;

public abstract class Figuras implements Drawable {

    abstract void getArea();

    abstract void getPerimetro();

    abstract void  getName();
}
