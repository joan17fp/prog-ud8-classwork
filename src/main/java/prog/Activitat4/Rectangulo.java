package prog.Activitat4;

public class Rectangulo extends Figuras {

    public int base;

    public int altura;

    public Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    public String getDrawing() {
        return "[]";
    }

    @Override
    public String getDrawing(Color color) {
        return color+"[]";
    }

    @Override
    public void setColor(Color color) {

    }

    @Override
    void getArea() {
        System.out.println("Area = " + (base * altura));
    }

    @Override
    void getName() {
        System.out.println("Rectangulo");
    }

    @Override
    void getPerimetro() {
        System.out.println("Perimetro= " + (2 * base + 2 * altura));
    }
}
