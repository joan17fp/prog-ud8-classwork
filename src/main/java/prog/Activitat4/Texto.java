package prog.Activitat4;

public class Texto implements Drawable {

    public String text;

    public Texto(String text) {
        this.text = text;
    }

    @Override
    public String getDrawing() {
        return text;
    }

    @Override
    public String getDrawing(Color color) {
        return text+color;
    }

    @Override
    public void setColor(Color color) {

    }

    public int getLenght(){

       return text.length();
    }

}
