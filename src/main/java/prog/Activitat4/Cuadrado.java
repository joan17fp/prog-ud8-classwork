package prog.Activitat4;

public class Cuadrado extends Figuras {

    public int altura;

    public Cuadrado(int altura) {

        this.altura = altura;
    }

    @Override
    void getPerimetro() {
        System.out.println("Perimetro = "+(4*altura));
    }

    @Override
    void getName() {
        System.out.println("Cuadrado");
    }

    @Override
    void getArea() {
        System.out.println("Area = "+(2*altura));
    }

    @Override
    public String getDrawing() {
        return "■";
    }

    @Override
    public String getDrawing(Color color) {
        return color+"■";
    }

    @Override
    public void setColor(Color color) {

    }
}

