package prog.Activitat4;

import java.util.Scanner;

public class Menu {

    private final int OPTION_RECTANG = 1;
    private final int OPTION_CIRCULO = 2;
    private final int OPTION_CUADRADO = 3;
    private final int OPTION_TEXTO = 4;
    private final int OPTION_EXIT_NEW = 5;

    private Scanner lector = new Scanner(System.in);

    public Drawable menu() {

        int respuesta;
        System.out.println("╔═════════════════════════════════╗");
        System.out.println("║ Bienvenido a Talleres Marcel·lí ║");
        System.out.println("╚═════════════════════════════════╝");
        System.out.println();

        do {
            System.out.println("------------------------");
            System.out.println("Eliga la opción deseada:");
            System.out.println("------------------------");
            System.out.println(OPTION_CUADRADO + " -> Dibujar un cuadrado.");
            System.out.println(OPTION_RECTANG + " -> Dibujar un rectangulo");
            System.out.println(OPTION_CIRCULO + " -> Dibujar un circulo.");
            System.out.println(OPTION_TEXTO + " -> Dibujar texto");
            respuesta = lector.nextInt();

            switch (respuesta) {
                case OPTION_CUADRADO:
                    newCuadrado();
                    break;
                case OPTION_RECTANG:
                    newRectangulo();
                    break;
                case OPTION_CIRCULO:

                    break;
                case OPTION_TEXTO:
                    newTexto();
                    break;
            }

        } while (respuesta != OPTION_EXIT_NEW);
        System.out.println("¡Que tenga un buen dia!");
        return null;
    }
    private Cuadrado newCuadrado(){

        System.out.println("Altura del cuadrado:");
        int altura = lector.nextInt();
       return new Cuadrado(altura);
    }
    private Rectangulo newRectangulo(){

        System.out.println("Altura del rectangulo:");
        int altura = lector.nextInt();
        System.out.println("Base del rectangulo:");
        int base = lector.nextInt();
        return new Rectangulo(base,altura);
    }

    private Texto newTexto(){

        System.out.println("Texto a introducir:");
        String text = lector.next();

        return new Texto(text);
    }



}
