package prog.Activitat2;

public class Pelicules extends Multimedia {

    private String actorPrincipal;

    private String actrizPrincipal;

    public Pelicules(String titol, String autor, int durada, String actrizPrincipal, Multimedia.formato formato) {
        super(titol, autor, durada, formato);
        this.actrizPrincipal =actrizPrincipal;
    }

    public Pelicules(String titol, String autor, int durada, Multimedia.formato formato, String actorPrincipal) {
        super(titol, autor, durada, formato);
        this.actorPrincipal = actorPrincipal;
    }

    public Pelicules(String titol, String autor, int durada, Multimedia.formato formato, String actorPrincipal, String actrizPrincipal) {
        super(titol, autor, durada, formato);
        this.actorPrincipal = actorPrincipal;
        this.actrizPrincipal = actrizPrincipal;
    }

    @Override
    public String toString() {
        return super.toString()+ "\nActor principal: "+actorPrincipal+ " \nActriz principal: " + actrizPrincipal;
    }
}
