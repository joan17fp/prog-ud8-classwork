package prog.Activitat2;

abstract public class Multimedia {

    private String titol;

    private String autor;

    private int durada;

    public enum formato {WAV, MP3, MIDI, AVI, MOV, MPG, CD_AUDIO, DVD}

    private formato formato;

    public Multimedia(String titol, String autor, int durada, formato formato) {
        this.titol = titol;
        this.autor = autor;
        this.durada = durada;
        this.formato = formato;
    }

    @Override
    public boolean equals(Object o) {

        Multimedia otherObject = (Multimedia) o;
        return  (otherObject.titol.equals(this.titol) && otherObject.autor.equals(this.autor));

    }


    @Override
    public String toString() {
        return "Titol: " + titol + "\n"+
                "Autor: " + autor + "\n" +
                "Durada: " + durada +"\n"+
                "Formato: " + formato;
    }
}
