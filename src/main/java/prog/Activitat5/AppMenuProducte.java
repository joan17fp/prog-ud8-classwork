package prog.Activitat5;

import java.util.Scanner;

public class AppMenuProducte {

    private final int OPTION_ADD_NEW = 1;
    private final int OPTION_MODIFY_PROD = 2;
    private final int OPTION_DELETE_PROD = 3;
    private final int OPTION_EXIT_NEW = 4;

    private Supermercat mercat;

    private Scanner lector = new Scanner(System.in);

    public AppMenuProducte(Supermercat mercat) {

        this.mercat = mercat;
    }

    public void menu() {

        int respuesta;
        System.out.println("╔════════════════════════════════════╗");
        System.out.println("║ Bienvenido a Supermercados Marcelí ║");
        System.out.println("╚════════════════════════════════════╝");
        System.out.println();

        do {
            System.out.println("------------------------");
            System.out.println("Eliga la opción deseada:");
            System.out.println("------------------------");
            System.out.println(OPTION_ADD_NEW + " -> Registrar un nuevo producto.");
            System.out.println(OPTION_MODIFY_PROD + " -> Modificar un producto");
            System.out.println(OPTION_DELETE_PROD + " -> Eliminar un producto.");
            System.out.println(OPTION_EXIT_NEW + " -> Salir del menú.");

            respuesta = lector.nextInt();

            switch (respuesta) {
                case OPTION_ADD_NEW:
                    mercat.registerNewProduct();
                    break;
                case OPTION_MODIFY_PROD:
                    mercat.modifyProduct();
                    break;
                case OPTION_DELETE_PROD:
                    mercat.removeProduct();
                    break;

            }

        } while (respuesta != OPTION_EXIT_NEW);
        System.out.println("¡Que tenga un buen dia!");
    }
}

