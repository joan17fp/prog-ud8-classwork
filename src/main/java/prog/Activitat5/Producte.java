package prog.Activitat5;

public class Producte implements Cloneable {

  private int code;

  private String name;

  private double basePrice;

  private double discount;

  private final double iva = 0.21;

  public String getName() {
    return name;
  }

  public int getCode() {
        return code;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getIva() {
        return iva;
    }

    public Producte(int code, String name, double basePrice, double discount) {

    this.code = code;
    this.name = name;
    this.basePrice = basePrice;
    this.discount = discount;
  }
  public double getPrize() {

    double withIva = getBasePrice() + (getBasePrice() * getIva());
    return withIva - (withIva * (getDiscount() / 100));

  }

  @Override
  protected Producte clone() throws CloneNotSupportedException {

    return (Producte) super.clone();

  }

  @Override
  public String toString() {

    return "Code: " + code + "\nNombre: " + name+"\n\n";
  }

}


