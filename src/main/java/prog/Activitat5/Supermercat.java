package prog.Activitat5;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

import java.util.ArrayList;
import java.util.Scanner;

public class Supermercat {

    private Scanner lector = new Scanner(System.in);

    private ArrayList<Producte> productList = new ArrayList<>();

    private ArrayList<Comanda> comandaList = new ArrayList<>();

     /*   Producte producte1 = new Producte(0,"agua",15,2);
        Producte producte2 = new Producte(1,"leche",10,4);
        Comanda comanda1 = new Comanda(0,productList,"12/10/19","joan");
    public void newOrd(){
        comandaList.add(comanda1);
    }
        public void newProd(){
            productList.add(producte1);
            productList.add(producte2);
        }*/

    public void registerNewProduct() {

        System.out.println("Introduce los datos del producto:");
        int code = productList.size();
        System.out.print("Nombre producto: ");
        String name = lector.next();
        System.out.print("Precio base: ");
        double basePrice = lector.nextDouble();
        System.out.print("Descuento: ");
        double discount = lector.nextDouble();
        Producte producte = new Producte(code, name, basePrice, discount);
        productList.add(producte);

    }

    public void modifyProduct() {

        listProducts();
        System.out.println("Escribe el codigo del producto que quieres modificar: ");
        int codes = lector.nextInt();
        Producte searchedProduct = searchProdByCode(codes);
        if (searchedProduct != null) {
            System.out.println("Introduce los datos del producto:");
            System.out.print("Precio base: ");
            productList.get(codes).setBasePrice(lector.nextDouble());
            System.out.print("Descuento: ");
            productList.get(codes).setDiscount(lector.nextDouble());

        } else {

            System.out.println("No hay ningun producto con ese código");
        }

    }

    private Producte searchProdByCode(int code) {

        for (Producte producte : productList) {
            if (producte.getCode() == code) {
                return producte;

            }

        }

        return null;
    }


    public void removeProduct() {

        System.out.println("Escribe el codigo  del producto a retirar:");
        int productoElegido = lector.nextInt();
        Producte searchedProduct = searchProdByCode(productoElegido);
        if (searchedProduct != null) {
            productList.remove(searchedProduct);
            System.out.println("Su producto ha sido retirado");
        } else {
            System.out.println("Su producto no existe");
        }
    }

    public void registerNewOrder() {

        ArrayList<Producte> orderProductList = new ArrayList<>();

        System.out.println("Introduce los datos del pedido:");
        int code = comandaList.size();

        System.out.print("Nombre cliente: ");
        String clientName = lector.next();

        System.out.print("Fecha: ");
        String date = lector.next();

        String respuesta;
        do {

            listProducts();
            System.out.println("Escribe el código del producto que desee: ");
            int productCode = lector.nextInt();
            try {
                orderProductList.add(productList.get(productCode).clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            System.out.println("¿Quieres añadir otro producto? (S/N)");
            respuesta = lector.next();

        } while (respuesta.equalsIgnoreCase("S"));

        Comanda comanda = new Comanda(code, orderProductList, date, clientName);
        comandaList.add(comanda);

    }

    public void listProducts() {

        AsciiTable at = new AsciiTable();
        at.addRule();
        at.addRow("Code", "Nombre del producto");
        for (Producte producte : productList) {
            at.addRule();
            at.addRow(producte.getCode(), producte.getName());
        }
        at.addRule();
        at.setTextAlignment(TextAlignment.CENTER);
        String rend = at.render();
        System.out.println(rend);

    }

    public void listComanda() {

        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow("Codigo Pedido", "Nombre Cliente", "Fecha del pedido");
        for (Comanda comanda : comandaList) {
            at.addRule();
            at.addRule();
            at.addRow(comanda.getCode(), comanda.getClientName(), comanda.getFecha());
            at.addRule();
            at.addRule();
            at.addRow(null,null,"Lista de productos");
            at.addRule();
            at.addRule();
            at.addRow("Codigo","Nombre","Precio Producto");
            at.addRule();

            for (Producte producte: comanda.getProductes()){

                at.addRow(producte.getCode(),producte.getName(),producte.getPrize()+" €");
                at.addRule();

            }
            at.addRow("Precio total",null,totalPrice()+" €");
        }

        at.addRule();
        at.setTextAlignment(TextAlignment.CENTER);
        at.setTextAlignment(TextAlignment.CENTER);
        String rend = at.render();
        System.out.println(rend);


    }

    public double totalPrice() {

        for (Comanda comanda : comandaList) {
            return Math.round(comanda.getTotalPrice() * 100.0) / 100.0;
        }
        return -1;
    }

}



