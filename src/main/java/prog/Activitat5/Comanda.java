package prog.Activitat5;

import java.util.ArrayList;


public class Comanda {

    private int code;

    private ArrayList<Producte> productes;

    private String fecha;

    private String clientName;

    public int getCode() {
        return code;
    }

    public ArrayList<Producte> getProductes() {
        return productes;
    }

    public String getFecha() {
        return fecha;
    }

    public String getClientName() {
        return clientName;
    }

    public Comanda(int code, ArrayList<Producte> productes, String fecha, String clientName) {
        this.code = code;
        this.productes = productes;
        this.fecha = fecha;
        this.clientName = clientName;
    }

    @Override
    public String toString() {

        return "Code: " + code + "\nClient: " + clientName + "\nProductos: " + productes + "\nFecha: " + fecha;
    }

    private double getPrize(Producte producte) {

        double withIva = producte.getBasePrice() + (producte.getBasePrice() * producte.getIva());
        return withIva - (withIva * (producte.getDiscount() / 100));

    }

    public double getTotalPrice() {
        double total = 0;
        for (int i = 0; i < productes.size(); i++) {
            total += getPrize(productes.get(i));
        }
        return total;
    }

}

