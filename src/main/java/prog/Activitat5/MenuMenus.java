package prog.Activitat5;

import java.util.Scanner;

public class MenuMenus {

        private final int OPTION_MENU_PROD = 1;
        private final int OPTION_MENU_COMAN = 2;
        private final int OPTION_EXIT_NEW = 3;

        private Scanner lector = new Scanner(System.in);

        private AppMenuProducte menuProducte;

        private AppMenuComandes menuComandes;

        public MenuMenus(AppMenuProducte menuProducte, AppMenuComandes menuComandes) {

            this.menuProducte = menuProducte;
            this.menuComandes = menuComandes;
        }

        public void menu() {

            int respuesta;
            System.out.println("╔════════════════════════════════════╗");
            System.out.println("║ Bienvenido a Supermercados Marcelí ║");
            System.out.println("╚════════════════════════════════════╝");
            System.out.println();

            do {
                System.out.println("------------------------");
                System.out.println("Eliga la opción deseada:");
                System.out.println("------------------------");
                System.out.println(OPTION_MENU_PROD + " -> Acceder al menu de productos");
                System.out.println(OPTION_MENU_COMAN + " -> Acceder al menu de pedidos");
                System.out.println(OPTION_EXIT_NEW + " -> Salir del menú.");

                respuesta = lector.nextInt();

                switch (respuesta) {
                    case OPTION_MENU_PROD:
                        menuProducte.menu();
                        break;
                    case OPTION_MENU_COMAN:
                        menuComandes.menu();
                        break;
                }

            } while (respuesta != OPTION_EXIT_NEW);
            System.out.println("¡Que tenga un buen dia!");

        }
    }

