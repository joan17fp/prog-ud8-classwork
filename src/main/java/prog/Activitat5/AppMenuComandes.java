package prog.Activitat5;

import java.util.Scanner;

public class AppMenuComandes {

    private final int OPTION_NEW_ORDER = 1;
    private final int OPTION_LIST_ORDER = 2;
    private final int OPTION_EXIT_NEW = 3;

    private Supermercat mercat;

    private Scanner lector = new Scanner(System.in);

    public AppMenuComandes(Supermercat mercat) {

        this.mercat = mercat;
    }

    public void menu() {

        int respuesta;
        System.out.println("╔════════════════════════════════════╗");
        System.out.println("║ Bienvenido a Supermercados Marcelí ║");
        System.out.println("╚════════════════════════════════════╝");
        System.out.println();

        do {
            System.out.println("------------------------");
            System.out.println("Eliga la opción deseada:");
            System.out.println("------------------------");
            System.out.println(OPTION_NEW_ORDER + " -> Hacer un pedido");
            System.out.println(OPTION_LIST_ORDER + " -> Ver mis pedidos");
            System.out.println(OPTION_EXIT_NEW + " -> Salir del menú.");

            respuesta = lector.nextInt();

            switch (respuesta) {

                case OPTION_NEW_ORDER:
                    mercat.registerNewOrder();
                    break;
                case OPTION_LIST_ORDER:
                    mercat.listComanda();
                    break;
            }

        } while (respuesta != OPTION_EXIT_NEW);
        System.out.println("¡Que tenga un buen dia!");
    }
}

